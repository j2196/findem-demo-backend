const mongoose = require('mongoose');
const {uri} = require("../utils/config");

module.exports = () => {
    try{
        mongoose.connect(uri);
        console.log('mongodb connected')
    }catch(error) {
        console.log(error);
        console.log('Could not connect to DB!')
    }
}