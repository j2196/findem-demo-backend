const express = require('express');
const router = express.Router();
const {User} = require("../../models/user");
const bcrypt = require('bcryptjs');

router.post('/register', (req, res) => {
    const {name, email, role, password} = req.body;

    //checking any user exists with email address
    User.findOne({email}, (error, user) => {
        if(user) {
            res.json({
                success: false,
                message: `User already exits with ${email}. Please select another email address`
            })
        } else {
            const encryptedPassword = bcrypt.hashSync(password, 12);

            const user = new User({
                name,
                email,
                role,
                password: encryptedPassword
            })
        
            user.save((error) => {
                if(error) {
                    res.json({
                        error
                    })
                } else {
                    res.json({success: true, message: 'User Registered Successfully', user})
                }
            })
        }
    })
})


module.exports = router;