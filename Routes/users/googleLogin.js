const express = require("express");
const router = express.Router();
const { User } = require("../../models/user");

router.post("/googleLogin", (req, res) => {

  const { email_verified, email, name } = req.body;

  if (email_verified) {
    // checking any user exists with email address
    User.findOne({ email }, (error, user) => {
      if (user) {
        req.session.user = user;
        res.status(200).json({
          success: true,
          message: "Login Successful",
          user,
        });
      } else if (error) {
        res.json({
          success: false,
          message: "Invalid email or password",
        });
      } else if (!user) {
        //If no user is present creating one with that credentials
        //creating password
        let password = email + "123456789";

        const user = new User({
          name,
          email,
          password,
        });
        user.save((error) => {
          if (error) {
            res.json({
              error,
            });
          } else {
            //user created and creating session for persistence
            req.session.user = user;
            res.json({
              success: true,
              message: "User Registered Successfully",
              user,
            });
          }
        });
      }
    });
  } else {
    res.json({
      success: false,
      message: "Invalid email or password",
    });
  }
});

module.exports = router;
