const express = require('express');
const router = express.Router();
const {User} = require("../../models/user");
const bcrypt = require('bcryptjs');
const {cookieExpirationRememberME} = require("../../utils/config");

router.post('/login', (req, res) => {
    const {email, password, rememberMe} = req.body;
    
    //checking whether user exists
    User.findOne({email}, (error, user) => {
        if(user) {
            //comparing passwords
            const isPasswordsMatched = bcrypt.compareSync(password, user.password);

            if(isPasswordsMatched) {
                //adding session cookie before sending the request
                //if user clicked on remember me, then login cookie session will be active for one year else one day
                if(rememberMe) {
                    req.session.cookie.maxAge = parseInt(cookieExpirationRememberME);
                }
                req.session.user = user;
                res.status(200).json({
                    success: true,
                    message: 'Login Successful',
                    user
                })
            } else {
                res.json({
                    success: false,
                    message: 'Invalid email or password'
                })
            }
        } else {
            res.json({
                success: false,
                message: 'Invalid email or password'
            })
        }
    })
})


module.exports = router;