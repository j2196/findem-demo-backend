const express = require('express');
const router = express.Router();

//this is session route from frontend
router.get('/auth', (req, res) => {
    if(req.session.user) {
        res.send({
            loggedIn: true,
            user: req.session.user
        })
    } else{
        res.send({
            loggedIn: false,
            user: ""
        })
    }
})

//remove cookie after user logs outs
router.get('/clearUserSession', (req, res) => {
    res.clearCookie("userId", {path: "/"});
    res.status(200).json({success: true, message: "User logged out successfully"})
})


module.exports = router;