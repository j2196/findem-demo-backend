const express = require('express');
const router = express.Router();
const Candidate = require("../../models/candidates");

router.get("/getData", async (req, res) => {
    let candidates = await Candidate.find();
    res.status(200).json({
        candidates
    })
});

module.exports = router;