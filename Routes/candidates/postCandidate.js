const express = require('express');
const router = express.Router();
const Candidate = require("../../models/candidates");

router.post("/post", (req, res) => {
    const newCandidate = new Candidate(req.body)
    newCandidate.save((error) => {
        if(!error) {
            res.status(201).json({success: true, message: "User created successfully"})
        } else {
            res.status(404).json({success: false, message: error.message})
        }
    })
})


module.exports = router;