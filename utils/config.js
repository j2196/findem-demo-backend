require('dotenv').config();

module.exports = {
    port: process.env.PORT,
    uri: process.env.URI,
    cookieSecret: process.env.COOKIE_SECRET,
    cookieExpiration: process.env.COOKIE_EXPIRATION,
    cookieExpirationRememberME: process.env.COOKIE_EXPIRATION_REMEMBERME
}