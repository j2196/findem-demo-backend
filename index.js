const express = require('express');
const app = express();
const cors = require('cors');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const {port, cookieSecret, cookieExpiration} = require('./utils/config');
const connectionDB = require("./db/connection");

//getting user Routes
const registerRoute = require("./Routes/users/register");
const loginRoute = require("./Routes/users/login");
const authRoute = require("./Routes/users/auth");
const getUsersRoute = require("./Routes/users/getUsersRoute");
const googleAuthLoginRoute = require("./Routes/users/googleLogin");

//getting candidates Route
const getCandidatesList = require("./Routes/candidates/getCandidates");
const postCandidateRoute = require("./Routes/candidates/postCandidate");


//middleware 
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cors({
    origin: ["http://localhost:3000"],
    methods: ["GET", "POST"],
    credentials: true
}));
app.use(cookieParser());

app.use(session({
    key: "userId",
    secret: cookieSecret,
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: parseInt(cookieExpiration)
    }
}))

//Db Connection
connectionDB();

//user Routes
app.use("/", [getUsersRoute, registerRoute, loginRoute, authRoute, googleAuthLoginRoute]);

//candidate Routes
app.use("/api/v1/candidates/", [getCandidatesList, postCandidateRoute]);

app.listen(port, () => console.log(`Server running on port ${port}`));