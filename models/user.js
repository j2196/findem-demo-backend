const mongoose = require('mongoose');

//schema
const userSchema = new mongoose.Schema({
    name: String,
    email: String,
    password: String,
    role: {
        type: String,
        default: 'Dev'
    }
})

//model
const User = new mongoose.model("User", userSchema);

module.exports = {User};