const mongoose = require('mongoose');

let candidateSchema = new mongoose.Schema({
    name: String,
    email: String,
    phone: String,
    linkedIn: String,
    currentDesignation: String,
    customer: String,
    shortlistedOn: String
})

let Candidate = new mongoose.model("Candidate", candidateSchema);

module.exports = Candidate;